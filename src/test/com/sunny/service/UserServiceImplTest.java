package com.sunny.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sunny.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-core-config.xml")
public class UserServiceImplTest {

	@Autowired
	IUserService userService;
	
	@Test
	public void testAddUser() {
		User user = new User();
		user.setAge("22");
		user.setName("sunny");
		userService.addUser(user);
	}

	@Test
	public void testDeleteUserById() {
		userService.deleteUserById(2);
	}

	@Test
	public void testUpdateUser() {
		User user = new User();
		user.setId(3);
		user.setAge("22");
		user.setName("sunny--------------");
		userService.updateUser(user);
	}

	@Test
	public void testFindUserById() {
		User user = userService.findUserById(3);
		System.err.println(user);
	}

	@Test
	public void testListUsers() {
		List<User> users = userService.listUsers();
		for (User user : users) {
			System.err.println(user);
		}
	}

}
