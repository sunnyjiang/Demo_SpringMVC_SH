package com.sunny.dao;

import java.util.List;

import com.sunny.entity.User;

public interface UserDao {
	public void add(User user);
	
	public void delete(int id);
	
	public void update(User user);
	
	public User find(int id);
	
	public List<User> list();
}
