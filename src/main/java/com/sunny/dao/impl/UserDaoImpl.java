package com.sunny.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.sunny.dao.UserDao;
import com.sunny.entity.User;

@Component("userDao")
public class UserDaoImpl implements UserDao{
	
	@Resource
	public SessionFactory sessionFactory;
	
	public void add(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
		
	}

	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		Object user = session.get(User.class, id);
		session.delete(user);
		
	}

	public void update(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}

	public User find(int id) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User)session.get(User.class, id);
		return user;
	}

	public List<User> list() {
		Session session = sessionFactory.getCurrentSession();
		List<User> users = (List<User>)session.createQuery("from User").list();
		System.out.println(users.size()+"~~~~~~~~~~~~~~~~~~~~~~~~");
		for (User user : users) {
			System.out.println(user);
		}
		return users;
	}

}
