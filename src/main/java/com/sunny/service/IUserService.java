package com.sunny.service;

import java.util.List;

import com.sunny.entity.User;

public interface IUserService {
	
	public void addUser(User user);
	
	public void deleteUserById(int id);
	
	public void updateUser(User user);
	
	public User findUserById(int id);
	
	public List<User> listUsers();
}
