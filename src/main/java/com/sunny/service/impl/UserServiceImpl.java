package com.sunny.service.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sunny.dao.UserDao;
import com.sunny.entity.User;
import com.sunny.service.IUserService;

@Component("userService")
@Transactional
public class UserServiceImpl implements IUserService{
	
	@Autowired
	UserDao userDao;
	
	
	public void addUser(User user) {
		userDao.add(user);
		
	}

	public void deleteUserById(int id) {
		userDao.delete(id);
		
	}

	public void updateUser(User user) {
		userDao.update(user);
		
	}


	public User findUserById(int id) {
		return userDao.find(id);
	}

	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return userDao.list();
	}

}
