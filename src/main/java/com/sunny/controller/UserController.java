package com.sunny.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sunny.entity.User;
import com.sunny.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Resource
	IUserService userService;
	
	@RequestMapping("add")
	public ModelAndView addUser(User user){
		System.out.println("add user="+user);
		ModelAndView mView = new ModelAndView("redirect:list");
		userService.addUser(user);
		return mView;
	}
	
	@RequestMapping("delete")
	public ModelAndView delete(int id){
		ModelAndView mView = new ModelAndView("redirect:list");
		userService.deleteUserById(id);
		return mView;
	}
	@RequestMapping("update")
	public ModelAndView update(User user){
		System.out.println("update user="+user);
		ModelAndView mView = new ModelAndView("redirect:list");
		userService.updateUser(user);
		return mView;
	}
	@RequestMapping("find")
	public ModelAndView find(int id){
		ModelAndView mView = new ModelAndView();
		userService.findUserById(id);
		return mView;
	}
	
	@RequestMapping("list")
	public ModelAndView list(){
		ModelAndView mView = new ModelAndView("userList");
		List<User> users = userService.listUsers();
		mView.addObject("users",users);
		return mView;
	}
}
