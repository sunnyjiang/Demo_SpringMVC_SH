<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
 <%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<base href="<%=basePath%>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function deleteUser(id){
		location.href="user/delete?id="+id;
	}
	function updateUser(id){
		var name = $("#newName").val();
		var age = $("#newAge").val();
		location.href="user/update?id="+id+"&name="+name+"&age="+age;
	}
</script>
</head>
<body>
	add user:
	<form action="user/add">
		<input type="text" name="name"/>
		<input type="text" name="age"/>
		<input type="submit" value="addUser">
	</form>
	user list：
	
	<table>
	<thead>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>age</th>
			<th>operator</th>
		</tr>
	</thead>
		<tbody>
		<c:forEach var="user" items="${users}">
			<tr>
				<th>${user.id }</th>
				<th>${user.name }</th>
				<th>${user.age }</th>
				<th><input type="button" onclick="deleteUser(${user.id })" value="delete"/></th>
				<th><input type="button" onclick="updateUser(${user.id })" value="update"/></th>
			</tr>	
		</c:forEach>
			
		</tbody>
	</table>
	update User:
	<form action="user/update">
		<input type="text" id="newName"/>
		<input type="text" id="newAge"/>
	</form>
</body>
</html>